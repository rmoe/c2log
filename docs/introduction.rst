Introduction
============

c2log parses logbook files created by the PM5.

Where are the logbook files?
----------------------------

Logbook files are created when you connect a USB drive to
the PM5 and either work out or transfer previous workouts to
the drive. They are stored on the drive at ``/Concept2/Logbook/``.


Additional information
----------------------

The formats of the binary files found in the logbook are described
in :doc:`file_formats/index`. Detailed API documentation is
also :doc:`available <api/modules>`.

  1. What is it?
     a. Explain what this library does
     b. Explain what you can do with it
  2. How to use it
     a. How to create logbook object
     b. Explain logbook interface
     c. Explain shortcomings
  3. Detailed file format explanations
     a. One rst for each file
  4. API documentation
