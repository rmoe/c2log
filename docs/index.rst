.. c2log documentation master file, created by
   sphinx-quickstart on Sat Apr 22 09:40:38 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to c2log's documentation!
=================================

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   introduction
   how-to-use
   file_formats/index
   api/modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
