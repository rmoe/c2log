c2log\.structures package
=========================

Submodules
----------

c2log\.structures\.base module
------------------------------

.. automodule:: c2log.structures.base
    :members:
    :undoc-members:
    :show-inheritance:

c2log\.structures\.fixed module
-------------------------------

.. automodule:: c2log.structures.fixed
    :members:
    :undoc-members:
    :show-inheritance:

c2log\.structures\.interval module
----------------------------------

.. automodule:: c2log.structures.interval
    :members:
    :undoc-members:
    :show-inheritance:

c2log\.structures\.user module
------------------------------

.. automodule:: c2log.structures.user
    :members:
    :undoc-members:
    :show-inheritance:

c2log\.structures\.variable module
----------------------------------

.. automodule:: c2log.structures.variable
    :members:
    :undoc-members:
    :show-inheritance:
    :exclude-members: split_type, split_count, split_size


Module contents
---------------

.. automodule:: c2log.structures
    :members:
    :undoc-members:
    :show-inheritance:
