c2log package
=============

Subpackages
-----------

.. toctree::

    c2log.structures

Submodules
----------

c2log\.fields module
--------------------

.. automodule:: c2log.fields
    :members:
    :undoc-members:
    :show-inheritance:
    :exclude-members: bytes_required, pack, unpack

c2log\.logbook module
---------------------

.. automodule:: c2log.logbook
    :members:
    :undoc-members:
    :show-inheritance:
    :exclude-members: workout_map
