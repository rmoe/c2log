File Formats
=================================

.. toctree::
   :maxdepth: 1

   deviceloginfo.bin
   favorites.bin
   logdataaccesstbl.bin 
   logdatastorage.bin
   logstrokeinfo.bin
   strokedataaccesstbl.bin
   strokedatastorage.bin
   userdynamic.bin
   userstatic.bin
